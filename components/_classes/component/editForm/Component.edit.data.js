"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _utils = _interopRequireDefault(require("./utils"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable max-len */
var _default = [
//   {
//   weight: 0,
//   type: 'checkbox',
//   label: 'Multiple Values',
//   tooltip: 'Allows multiple values to be entered for this field.',
//   key: 'multiple',
//   input: true
// }, 
{
  type: 'select',
  label: 'Data Value',
  key: 'defaultValue',
  weight: 20,
  placeholder: 'Data Value',
  tooltip: 'The will be the value for this field, before user interaction. Having a default value will override the placeholder text.',
  template: '<span>{{ item.label }}</span>',
  data: {
    values: [{
      value: 'assessorInitial',
      label: 'Assessor Initial'
    }, {
      value: 'assessorInitialList',
      label: 'Assessor Initial List'
    },
    {
      value: 'assessorName',
      label: 'Assessor Name'
    }, {
      value: 'assessorSignature',
      label: 'Assessor Signature'
    },  {
      value: 'assessorSignatureList',
      label: 'Assessor Signature List'
    },{
      value: 'clientAllergies',
      label: 'Client Allergies'
    }, {
      value: 'dateOfBirth',
      label: 'Client Date of Birth'
    },  {
      value: 'clientName',
      label: 'Client Name'
    },  {
      value: 'clientPhone',
      label: 'Client Phone'
    },  {
      value: 'clientPharmacy',
      label: 'Client Pharmacy'
    }, {
      value: 'todayDate',
      label: 'Today\'s Date'
    }, {
      value: 'medicationDate',
      label: 'Medication Date'
    },   {
      value: 'medicationDosage',
      label: 'Medication Dosage'
    },   {
      value: 'medicationFrequency',
      label: 'Medication Frequency'
    }, {
      value: 'medicationGivenFor',
      label: 'Medication Given For'
    }, {
      value: 'medicationName',
      label: 'Medication Name'
    }, {
      value: 'medicationPrecautions',
      label: 'Medication Precautions'
    }, {
      value: 'medicactionRoute',
      label: 'medicationRoute'
    }, {
      value: 'medicationTime',
      label: 'Medication Time'
    },  {
      value: 'inappropriateDrugReactions',
      label: 'Medication Inappropriate Drug Reactions'
    }, {
      value: 'ineffectiveDrugTherapy',
      label: 'Medication Ineffective Drug Therapy'
    }, {
      value: 'foodDrugInteractions',
      label: 'Medication Food & Drug Interactions'
    },  {
      value: 'medicationChangeDate',
      label: 'Medication Change Date'
    },{
      value: 'medicationUsage',
      label: 'Medication Usage'
    },//supervisory visit
    {
      value: 'purposeOfVisit',
      label: 'Purpose of Visit'
    },
    {
      value: 'visitDate',
      label: 'Visit Date'
    },
    {
      value: 'timeIn',
      label: 'In Time'
    },
    {
      value: 'timeOut',
      label: 'Out Time'
    },
    {
      value: 'teachingAndTrainingProvided',
      label: 'List specifics'
    },
    {
      value: 'nurseCompletingVisit ',
      label: 'Nurse Name'
    },
    {
      value: 'skilledNursingCareClientSignature',
      label: 'Client Digital Signature'
    },
    {
      value: 'skilledNursingNurseSignature',
      label: 'Nurse Digital Signature'
    },
    {
      value: 'skilledNursingCareClientSignatureDate',
      label: 'Client Signature Date'
    },
    {
      value: 'skilledNursingNurseSignatureDate',
      label: 'Nurse Signature Date'
    },
    {
      value: 'generalNotes',
      label: 'Notes on Visit Outcome'
    },
    {
      value: 'bloodPressureInformation',
      label: 'Blood Pressure'
    },
    {
      value: 'glucose',
      label: 'Glucose'
    },
    {
      value: 'currentPulseReading',
      label: 'Pulse'
    },
    {
      value: 'pulseOxygenReading',
      label: 'PulseOx'
    },
    {
      value: 'currentRespirationsReading',
      label: 'Respiration'
    },
    {
      value: 'currentTemperatureReading',
      label: 'Temperature'
    },
    {
      value: 'bPNotes',
      label: 'Blood Pressure Notes'
    },
    {
      value: 'glucoseNotes',
      label: 'Glucose Notes'
    },
    {
      value: 'pulseNotes',
      label: 'Pulse Notes'
    },
    {
      value: 'respirationNotes',
      label: 'Respiration Notes'
    },
    {
      value: 'temperatureNotes',
      label: 'Temperature Notes'
    },
    {
      value: 'pulseOxygenNotes',
      label: 'PulseOx Notes'
    },
    {
      value: 'skilledNursingMedication',
      label: 'Medication Management'
    },
    {
      value: 'agencyLogoImage',
      label: 'Agency Logo'
    },
    {
      value: 'clientFullAddress',
      label: 'CL_Full Address'
    },
    {
      value: 'diagnosis',
      label: 'CL_Diagnosis'
    },
    //caregiver profile
    {
      value: 'cgfirstName',
      label: 'CG_First Name'
    },
    {
      value: 'cglastName',
      label: 'CG_Last Name'
    },
    {
      value: 'cgoffice',
      label: 'CG_Office Notes'
    },
    {
      value: 'cggender',
      label: 'CG_Gender'
    },
    {
      value: 'cgdob',
      label: 'CG_DOB'
    },
    {
      value: 'cgagencyLocation',
      label: 'CG_Location'
    },
    {
      value: 'maritalStatus',
      label: 'CG_Marital Status'
    },
    {
      value: 'caregiverStatus',
      label: 'CG_Enrolment Status'
    },
    {
      value: 'cgspouseName',
      label: 'CG_Spouse Name'
    },
    {
      value: 'cgisVeteran',
      label: 'CG_Is Veteran'
    },
    {
      value: 'cgstartOfCase',
      label: 'CG_Hire Date'
    },
    {
      value: 'cgaddress',
      label: 'CG_Address 1'
    },
    {
      value: 'cgaddressLine1',
      label: 'CG_Address 2'
    },
    {
      value: 'cgcity',
      label: 'CG_City'
    },
    {
      value: 'cgstate',
      label: 'CG_State'
    },
    {
      value: 'cgcountry',
      label: 'CG_Country'
    },
    {
      value: 'cgpostalCode',
      label: 'CG_Zip Code'
    },
    {
      value: 'cghomePhone',
      label: 'CG_Home Phone'
    },
    {
      value: 'cgofficePhone',
      label: 'CG_Office Phone'
    },
    {
      value: 'cgmobilePhone',
      label: 'CG_Mobile Phone'
    },
    {
      value: 'cgfax',
      label: 'CG_Fax'
    },
    {
      value: 'cgemail',
      label: 'CG_Email Address'
    },
    {
      value: 'cgimageUrl',
      label: 'CG_Profile Image'
    },
    {
      value: 'cgreferralType',
      label: 'CG_Referred Type'
    },
    {
      value: 'cgreferralBy',
      label: 'CG_Referred By'
    },
    {
      value: 'cgadpid',
      label: 'CG_ADP ID'
    },
    {
      value: 'cgFullName',
      label: 'CG_Full Name'
    },
    {
      value: 'cgFullAddress',
      label: 'CG_Full Address'
    },
    //employee profile
    {
      value: 'empFirstName',
      label: 'EMP_First Name'
    },
    {
      value: 'empLastName',
      label: 'EMP_Last Name'
    },
    {
      value: 'empGender',
      label: 'EMP_Gender'
    },
    {
      value: 'empDateofJoining',
      label: 'EMP_Date of Joining'
    },
    {
      value: 'empAddress1',
      label: 'EMP_Address Line 1'
    },
    {
      value: 'empAddress2',
      label: 'EMP_Address Line 2'
    },
    {
      value: 'empCity',
      label: 'EMP_City'
    },
    {
      value: 'empState',
      label: 'EMP_State'
    },
    {
      value: 'empCountry',
      label: 'EMP_Country'
    },
    {
      value: 'empZipCode',
      label: 'EMP_Zip Code'
    },
    {
      value: 'empMobilePhone',
      label: 'EMP_Mobile Phone'
    },
    {
      value: 'empEmail',
      label: 'EMP_Email'
    },
    {
      value: 'empFullName',
      label: 'EMP_Full Name'  
    },
    {
      value: 'empFullAddress',
      label: 'EMP_Full Address' 
    }
]
  },
  defaultValue: '',
  input: true
},
//  {
//   weight: 30,
//   type: 'radio',
//   label: 'Persistent',
//   tooltip: 'A persistent field will be stored in database when the form is submitted.',
//   key: 'persistent',
//   input: true,
//   inline: true,
//   defaultValue: true,
//   values: [{
//     label: 'None',
//     value: false
//   }, {
//     label: 'Server',
//     value: true
//   }, {
//     label: 'Client',
//     value: 'client-only'
//   }]
// }, {
//   weight: 150,
//   type: 'checkbox',
//   label: 'Protected',
//   tooltip: 'A protected field will not be returned when queried via API.',
//   key: 'protected',
//   input: true
// }, {
//   type: 'checkbox',
//   input: true,
//   weight: 200,
//   key: 'dbIndex',
//   label: 'Database Index',
//   tooltip: 'Set this field as an index within the database. Increases performance for submission queries.'
// }, {
//   weight: 400,
//   type: 'checkbox',
//   label: 'Encrypted (Enterprise Only)',
//   tooltip: 'Encrypt this field on the server. This is two way encryption which is not suitable for passwords.',
//   key: 'encrypted',
//   input: true
// }, 
// {
//   type: 'select',
//   input: true,
//   key: 'refreshOn',
//   label: 'Redraw On',
//   weight: 600,
//   tooltip: 'Redraw this component if another component changes. This is useful if interpolating parts of the component like the label.',
//   dataSrc: 'custom',
//   valueProperty: 'value',
//   data: {
//     custom: function custom(context) {
//       var values = [];
//       values.push({
//         label: 'Any Change',
//         value: 'data'
//       });
//       context.utils.eachComponent(context.instance.options.editForm.components, function (component, path) {
//         if (component.key !== context.data.key) {
//           values.push({
//             label: component.label || component.key,
//             value: path
//           });
//         }
//       });
//       return values;
//     }
//   },
//   conditional: {
//     json: {
//       nin: [{
//         var: 'data.type'
//       }, ['select']]
//     }
//   }
// },
//  {
//   weight: 700,
//   type: 'checkbox',
//   label: 'Clear Value When Hidden',
//   key: 'clearOnHide',
//   defaultValue: true,
//   tooltip: 'When a field is hidden, clear the value.',
//   input: true
// }, _utils.default.javaScriptValue('Custom Default Value', 'customDefaultValue', 'customDefaultValue', 1000, '<p><h4>Example:</h4><pre>value = data.firstName + " " + data.lastName;</pre></p>', '<p><h4>Example:</h4><pre>{"cat": [{"var": "data.firstName"}, " ", {"var": "data.lastName"}]}</pre>'), _utils.default.javaScriptValue('Calculated Value', 'calculateValue', 'calculateValue', 1100, '<p><h4>Example:</h4><pre>value = data.a + data.b + data.c;</pre></p>', '<p><h4>Example:</h4><pre>{"sum": [{"var": "data.a"}, {"var": "data.b"}, {"var": "data.c"}]}</pre><p><a target="_blank" href="http://formio.github.io/formio.js/app/examples/calculated.html">Click here for an example</a></p>'), {
//   type: 'checkbox',
//   input: true,
//   weight: 1200,
//   key: 'allowCalculateOverride',
//   label: 'Allow Manual Override of Calculated Value',
//   tooltip: 'When checked, this will allow the user to manually override the calculated value.'
// }
];
/* eslint-enable max-len */

exports.default = _default;