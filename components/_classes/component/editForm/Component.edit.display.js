"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

/* eslint-disable max-len */
var _default = [{
  weight: 0,
  type: 'textfield',
  input: true,
  key: 'label',
  label: 'Label',
  placeholder: 'Field Label',
  tooltip: 'The label for this field that will appear next to it.',
  validate: {
    required: true
  }
},  {
  weight: 100,
  type: 'textfield',
  input: true,
  key: 'placeholder',
  label: 'Placeholder',
  placeholder: 'Placeholder',
  tooltip: 'The placeholder text that will appear when this field is empty.'
},  {
  weight: 300,
  type: 'textarea',
  input: true,
  key: 'tooltip',
  label: 'Tooltip',
  placeholder: 'To add a tooltip to this field, enter text here.',
  tooltip: 'Adds a tooltip to the side of this field.'
},  {
  weight: 1370,
  type: 'checkbox',
  label: 'Show Label in DataGrid',
  tooltip: 'Show the label when in a Datagrid.',
  key: 'dataGridLabel',
  input: true,
  customConditional: function customConditional(context) {
    return context.instance.options.editComponent.inDataGrid;
  }
}];
/* eslint-enable max-len */

exports.default = _default;